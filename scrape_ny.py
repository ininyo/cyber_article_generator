#!/usr/bin/env python
import requests
import itertools
import multiprocessing.dummy as mp
import time
from bs4 import BeautifulSoup
import argparse

NY_TIME_CYBER = 'https://www.nytimes.com/svc/collections/v1/publish/topics.nytimes.com/topic/subject/computer-security-cybersecurity'

class NewYorkScraper(object):
    def __init__(self,link, file_name, max_threads):
        self.link = link
        self.file_name = file_name
        self.max_threads = max_threads

    def run(self):
        self.text_queue = mp.Queue()
        self.consumer_thread = mp.Process(target=self.append_to_file)
        self.consumer_thread.start()
        pool = mp.Pool(processes = self.max_threads)
        for i in itertools.count(): 
            items_json = None
            params = {'page' : i}
            max_retries = 3
            count = 0
            while items_json is None:
                try:
                    items_json = requests.get(self.link, params).json()['members']['items']
                except Exception as e:
                    count+=1
                    if (count > 3):
                        print e
                        continue
                    time.sleep(1)

            links = [item['url'] for item in items_json]
            if not links:
                break
            pool.map_async(self.get_page_and_append, links)

        pool.close()
        pool.join()
        self.text_queue.put('STOP')
        self.consumer_thread.join()

    def get_page_and_append(self, link):
        print 'downloading: ' + link
        article = None
        max_retries = 3
        count = 0

        while article is None:
            try:
                article = BeautifulSoup(requests.get(link).text)
            except Exception as e:
                count+=1
                if (count > 3):
                    print e
                    return
                time.sleep(1)

        title = article.find('title').text.encode('UTF-8')

        def _encode_and_add_line(p):
            return p.text.encode('UTF-8') + '\n'
        content = "".join([_encode_and_add_line(p) for p in article.findAll('p', {'class':'story-body-text'})])

        article_text = title + '\n' + content + '\n'
        self.text_queue.put(article_text)

    def append_to_file(self):
        with open(self.file_name, 'w++') as f:
            while True:
                article = self.text_queue.get()
                if 'STOP' == article:
                    break
                f.write(article)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('out', help='path of out file') 
    parser.add_argument('max_threads', help='maximum number of threads', type=int) 
    args = parser.parse_args()
    scraper = NewYorkScraper(NY_TIME_CYBER, args.out, args.max_threads)
    scraper.run()

